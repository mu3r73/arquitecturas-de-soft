import unittest
from fuente import Fuente


class TestFuente(unittest.TestCase):
  def setUp(self):
    self.fuente = Fuente('fuente.txt', 10)
    
  # falla si no existe un archivo con el nombre indicado
  def test_sin_archivo(self):
    with self.assertRaisesRegex(FileNotFoundError, 'No such file or directory'):
      f = Fuente('fuente_no_esta', 10)

  # falla si no se indica la cantidad de símbolos
  def test_sin_num_simbolos(self):
    with self.assertRaisesRegex(TypeError, 'missing 1 required positional argument'):
      f = Fuente('fuente.txt')
  
  # tests de dibujos para cada dígito
  def test_get_0(self):
    dibujo = self.fuente.get(0)
    self.assertEqual(dibujo[0], ' _ ')
    self.assertEqual(dibujo[1], '| |')
    self.assertEqual(dibujo[2], '|_|')
  
  def test_get_1(self):
    dibujo = self.fuente.get(1)
    self.assertEqual(dibujo[0], '   ')
    self.assertEqual(dibujo[1], '  |')
    self.assertEqual(dibujo[2], '  |')

  def test_get_2(self):
    dibujo = self.fuente.get(2)
    self.assertEqual(dibujo[0], ' _ ')
    self.assertEqual(dibujo[1], ' _|')
    self.assertEqual(dibujo[2], '|_ ')

  def test_get_3(self):
    dibujo = self.fuente.get(3)
    self.assertEqual(dibujo[0], ' _ ')
    self.assertEqual(dibujo[1], ' _|')
    self.assertEqual(dibujo[2], ' _|')

  def test_get_4(self):
    dibujo = self.fuente.get(4)
    self.assertEqual(dibujo[0], '   ')
    self.assertEqual(dibujo[1], '|_|')
    self.assertEqual(dibujo[2], '  |')

  def test_get_5(self):
    dibujo = self.fuente.get(5)
    self.assertEqual(dibujo[0], ' _ ')
    self.assertEqual(dibujo[1], '|_ ')
    self.assertEqual(dibujo[2], ' _|')

  def test_get_6(self):
    dibujo = self.fuente.get(6)
    self.assertEqual(dibujo[0], ' _ ')
    self.assertEqual(dibujo[1], '|_ ')
    self.assertEqual(dibujo[2], '|_|')

  def test_get_7(self):
    dibujo = self.fuente.get(7)
    self.assertEqual(dibujo[0], ' _ ')
    self.assertEqual(dibujo[1], '  |')
    self.assertEqual(dibujo[2], '  |')

  def test_get_8(self):
    dibujo = self.fuente.get(8)
    self.assertEqual(dibujo[0], ' _ ')
    self.assertEqual(dibujo[1], '|_|')
    self.assertEqual(dibujo[2], '|_|')

  def test_get_9(self):
    dibujo = self.fuente.get(9)
    self.assertEqual(dibujo[0], ' _ ')
    self.assertEqual(dibujo[1], '|_|')
    self.assertEqual(dibujo[2], ' _|')


if __name__ == '__main__':
	unittest.main()
