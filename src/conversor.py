class Conversor:

  @staticmethod
  def convertir(num, fuente):
    num_str = str(num)
    # arma lista con dibujos para cada dígito de num
    dibujos = [fuente.get(int(num)) for num in num_str]
    # recordar: cada dibujo es una tupla de 3 líneas
    # arma las líneas de resultado, juntando las líneas correspondientes de los dibujos
    res = [
      ''.join(dibujo[i] for dibujo in dibujos) # '' porque no se usa separación entre dibujos
      for i in range(0, 3) # 3 porque la fuente es de 3 líneas
    ]
    # retorna el resultado como 1 solo string de 3 líneas separadas con '\n'
    return '\n'.join(res)


# main para /ver/ si convierte ok
if __name__ == '__main__':
  from fuente import Fuente
  f = Fuente('fuente.txt', 10)
  res = Conversor.convertir(141421, f)
  print(res)
