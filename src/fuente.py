class Fuente:
  def __init__(self, nombre_archivo, num_simbolos):
    self.__cargar_fuente(nombre_archivo, num_simbolos)
  
  # carga una fuente desde el archivo cuyo nombre se indica,
  # y la manda a procesar
  # - nombre_archivo: archivo que contiene la fuente a cargar
  # - num_símbolos: normalmente 10, 1 símbolo por dígito
  def __cargar_fuente(self, nombre_archivo, num_simbolos):
    with open(nombre_archivo) as f:
      lineas = f.read().splitlines()
      self.fuente = self.preparar_fuente(lineas, num_simbolos)
  
  # dado un string 'lineas' que representa a una secuencia de caracteres,
  # y sabiendo que representa num_simbolos caracteres (1 símbolo por dígito),
  # arma la tupla self.fuente,
  # que tendrá 1 'dibujo' de 3 líneas para cada número
  def preparar_fuente(self, lineas, num_simbolos):
    ancho_letra = len(lineas[0]) // num_simbolos
    lineas_sep = [self.__separar_linea(linea, ancho_letra) for linea in lineas]
    return tuple([self.__armar_dibujo(lineas_sep, num) for num in range(0, num_simbolos)])
  
  # separa la línea en una lista de substrings de ancho ancho_letra
  def __separar_linea(self, linea, ancho_letra):
    return [linea[i : i+ancho_letra] for i in range(0, len(linea), ancho_letra)]
  
  # dada una lista de líneas (separadas en substrings de ancho ancho_letra),
  # y un número num,
  # retorna el dibujo de 3 líneas correspondiente a num
  def __armar_dibujo(self, lineas_sep, num):
    return (lineas_sep[0][num], lineas_sep[1][num], lineas_sep[2][num])
  
  # dado un número num, retorna el dibujo correspondiente
  # (dibujo: tupla de 3 strings, 1 por línea)
  def get(self, num):
    return self.fuente[num]


# main para /ver/ si la fuente se cargó ok
if __name__ == '__main__':
  f = Fuente('fuente.txt', 10)
  for num in range(0, 10):
    print('\n'.join(f.get(num)))
