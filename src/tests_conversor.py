import unittest
from fuente import Fuente
from conversor import Conversor
from random import randrange


class TestConversor(unittest.TestCase):
  def setUp(self):
    self.fuente = Fuente('fuente.txt', 10)
  
  # falla si no se le pasan argumentos
  def test_sin_args(self):
    with self.assertRaisesRegex(TypeError, 'missing 2 required positional arguments'):
      Conversor.convertir()

  # falla si no se le pasan dos argumentos
  def test_falta_1_arg(self):
    with self.assertRaisesRegex(TypeError, 'missing 1 required positional argument'):
      Conversor.convertir(123)

  # falla si no se le pasa un número como argumento
  def test_sin_numero(self):
    with self.assertRaisesRegex(ValueError, 'invalid literal for int'):
      Conversor.convertir(None, self.fuente)

  # falla si se le pasa algo que no es un número
  def test_arg_no_es_numero(self):
    with self.assertRaisesRegex(ValueError, 'invalid literal for int'):
      Conversor.convertir('abc', self.fuente)
  
  # tests de algunos números de varios dígitos
  def test_123(self):
    res = Conversor.convertir(123, self.fuente)
    esperado = '\n'.join([
      '    _  _ ',
      '  | _| _|',
      '  ||_  _|',
    ])
    self.assertEqual(res, esperado)
        
  def test_1984(self):
    res = Conversor.convertir(1984, self.fuente)
    esperado = '\n'.join([
      '    _  _    ',
      '  ||_||_||_|',
      '  | _||_|  |',
    ])
    self.assertEqual(res, esperado)
    
    self.assertEqual(res, esperado)
    
  def test_90125(self):
    res = Conversor.convertir(90125, self.fuente)
    esperado = '\n'.join([
      ' _  _     _  _ ',
      '|_|| |  | _||_ ',
      ' _||_|  ||_  _|',
    ])
    self.assertEqual(res, esperado)

  def test_31415926(self):
    res = Conversor.convertir(31415926, self.fuente)
    esperado = '\n'.join([
      ' _           _  _  _  _ ',
      ' _|  ||_|  ||_ |_| _||_ ',
      ' _|  |  |  | _| _||_ |_|',
    ])
    self.assertEqual(res, esperado)

  def test_4815162342(self):
    res = Conversor.convertir(4815162342, self.fuente)
    esperado = '\n'.join([
      '    _     _     _  _  _     _ ',
      '|_||_|  ||_   ||_  _| _||_| _|',
      '  ||_|  | _|  ||_||_  _|  ||_ ',
    ])
    self.assertEqual(res, esperado)

  # testea 500 números aleatorios
  def test_numeros_aleatorios(self):
    for i in range(0, 500):
      self.__test_num(randrange(99999999999999999999))
  
  # test genérico para cualquier número
  def __test_num(self, num):
    # obtengo el string que representa a num como dibujo
    dibujo_completo = Conversor.convertir(num, self.fuente).split('\n')
    # num como string, para poder iterar sobre sus dígitos
    num_str = str(num)
    # armo pseudo-fuente, como tupla con los dibujos (separados) de cada dígito de num
    pseudo_fuente_dibujo = self.fuente.preparar_fuente(dibujo_completo, len(num_str))
    # para cada dibujo de un dígito, verifico que sea el que le corresponde en la fuente original
    for n, dibujo_numero in enumerate(pseudo_fuente_dibujo):
      self.assertEqual(dibujo_numero, self.fuente.get(int(num_str[n])))
    
  
if __name__ == '__main__':
	unittest.main()
